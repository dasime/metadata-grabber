import virtual from "@rollup/plugin-virtual";
import cleanup from "rollup-plugin-cleanup";
import clear from "rollup-plugin-clear";
import copy from "rollup-plugin-copy";

import {detectPlugins} from "./src/detectPlugins.js";


const buildTarget = "build";

// Metadata Grabber plugin discovery and reporting

const detectedPlugins = await detectPlugins("./src/plugins/");
const detectedUniquePlugins = [
  ...new Set(detectedPlugins.map(plugin => plugin.filename)),
];
const detectedUniqueHosts = [
  ...new Set(detectedPlugins.map(plugin => plugin.hostname)),
];

console.log(`
Found ${detectedUniquePlugins.length} Metadata Grabber plugins \
for ${detectedUniqueHosts.length} hosts:
`);
console.table(detectedPlugins);

// Regular Rollout lifecycle

console.log("\nBundling Metadata Grabber...\n");

export default [
  {
    input: "src/sidebar/panel.js",
    output: {
      sourcemap: false,
      dir: `${buildTarget}/sidebar/`,
      format: "esm",
      generatedCode: {
        constBindings: true,
      },
    },
    plugins: [
      virtual({
        // Metadata Grabber plugin injection
        enabledPlugins:
          `export default ${JSON.stringify(detectedPlugins, null, 2)}`,
      }),
      cleanup({
        comments: "none",
        maxEmptyLines: 1,
      }),
      clear({
          // required, point out which directories should be clear.
          targets: [buildTarget],
      }),
      copy({
        targets: [
          {
            src: "src/sidebar/*.html",
            dest: `${buildTarget}/sidebar`,
          },
          {
            src: "src/sidebar/*.css",
            dest: `${buildTarget}/sidebar`,
          },
        ],
      }),
    ],
    // watch: {
    //   clearScreen: true,
    // },
  },

  ...detectedUniquePlugins.map(filename => {
    return {
      input: `src/plugins/${filename}`,
      output: {
        sourcemap: false,
        dir: `${buildTarget}/plugins`,
        format: "esm",
        generatedCode: {
          constBindings: true,
        },
      },
      plugins: [
        cleanup({
          comments: "none",
          maxEmptyLines: 1,
        }),
      ],
    };
  }),

];
