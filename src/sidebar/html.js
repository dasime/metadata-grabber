/*
 * Misc
 */

function attachElementHandler(element, handler) {
  if (element.addEventListener) {
    element.addEventListener("click", handler, false);
  } else if (element.attachEvent) {
    element.attachEvent("onclick", handler);
  }
}

function createElement(tagName, {...attributes} = {}) {
  const el = document.createElement(tagName);

  Object.keys(attributes).forEach(key => {
    el.setAttribute(key, attributes[key]);
  });

  return el;
}

function generateElementUID() {
  // Not secure, just enough to tell two different elements apart
  return Math.random().toString().slice(-8);
}

function removeChildElements(parentElement) {
  while (parentElement.lastChild) {
    parentElement.removeChild(parentElement.lastChild);
  }
}

// Remove 1..n elements by their 'id' attribute
function removeElementsById(...elementIds) {
  for (const elementId of elementIds) {
    document.getElementById(elementId).remove();
  }
}

export {
  attachElementHandler,
  createElement,
  generateElementUID,
  removeChildElements,
  removeElementsById,
};

/*
 * Forms
 */

/**
 * Convert FormData to JSON
 *
 * Notes on formData serialising:
 * https://stackoverflow.com/questions/41431322/how-to-convert-formdatahtml5-object-to-json
 *
 * Source: https://stackoverflow.com/a/71315057
 */
function form2json(formData) {
  const method = function (object, pair) {
    const keys = pair[0].replace(/\]/g,"").split("[");
    const key = keys[0];
    let value = pair[1];

    if (keys.length > 1) {
      let i,x,segment;
      const last = value;
      const type = isNaN(keys[1]) ? {} : [];

      value = segment = object[key] || type;

      for (i = 1; i < keys.length; i++) {
        x = keys[i];

        if (i == keys.length-1) {
          if (Array.isArray(segment)) {
            segment.push(last);
          } else {
            segment[x] = last;
          }
        } else if (segment[x] == undefined) {
          segment[x] = isNaN(keys[i+1]) ? {} : [];
        }
        segment = segment[x];
      }
    }
    object[key] = value;

    return object;
  };
  return Array.from(formData).reduce(method,{});
}


export {
  form2json,
};
