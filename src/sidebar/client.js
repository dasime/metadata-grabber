// NOTE: The "enabledPlugins" module is generated during the build process.
// eslint-disable-next-line import/no-unresolved
import plugins from "enabledPlugins";

let myWindowId;

function setWindowId(windowId) {
  myWindowId = windowId;
}

async function getCurrentTab() {
  return await browser.tabs.query({windowId: myWindowId, active: true});
}

async function detectPluginsForCurrentUrl() {
  const tabs = await getCurrentTab();
  const url = tabs[0].url;

  if (url) {
    const hostname = new URL(url).hostname;
    const matchedPlugins = plugins.filter(p => p.hostname === hostname);

    return [hostname, matchedPlugins];
  } else {
    // Without the `tabs` permission and only the `activeTabs` permission
    // "about:*" addresses return without a url property.
    return ["Internal Page", []];
  }
}

async function runPlugin(filename) {
  return await browser.tabs.executeScript({
    file: `../plugins/${filename}`,
  });
}

export {detectPluginsForCurrentUrl, runPlugin, getCurrentTab, setWindowId};
