/*
 * The 'Netscape Cookie File' is supported by several tools,
 * including: 'curl', 'wget', 'aria2' and 'youtube-dl' forks.
 *
 * This makes it useful for authenticating to download larger artefacts
 * outside of the browser with more suitable tools.
 *
 * Notes on this standard:
 * - https://curl.se/docs/http-cookies.html
 * - https://github.com/yt-dlp/yt-dlp/wiki/FAQ
*/

import {getCurrentTab} from "./client.js";

async function getCookiesForCurrentTab() {
  const tabs = await getCurrentTab();

  return await browser.cookies
    .getAll({
      storeId: tabs[0].cookieStoreId,
      // required when browser has first-party isolation configured
      firstPartyDomain: null,
    });
}

/*
 *  Original 'formatCookie' function from 'cookies-txt'
 *  https://github.com/hrdl-github/cookies-txt/blob/master/background.js
 *  Copyright Lennon Hill - licensed under GPLv3
 *
 *  Some changes made, but it's basically the same.
 */
function formatCookie(co) {
  return [
    [
      !co.hostOnly && co.domain && !co.domain.startsWith(".") ? "." : "",
      co.domain,
    ].join(""),
    co.hostOnly ? "FALSE" : "TRUE",
    co.path,
    co.secure ? "TRUE" : "FALSE",
    co.session || !co.expirationDate ? 0 : co.expirationDate,
    co.name,
    `${co.value  }`,
  ].join("\t");
}

async function getBrowserData() {
  const response = {};

  // Fetch user agent
  response.userAgent = window.navigator.userAgent;

  // Fetch and format cookies
  const header = ["# Netscape HTTP Cookie File"];
  const cookies = await getCookiesForCurrentTab();
  const cookiesPrint = cookies.map(formatCookie);

  response.cookies = header.concat(cookiesPrint).join("\n");

  return response;
}

function writeCookies(cookies) {
  browser.downloads.download({
    url: URL.createObjectURL(new Blob([ cookies ])),
    filename: "cookies.txt",
    saveAs: true,
  });
}

export {getBrowserData, writeCookies};
