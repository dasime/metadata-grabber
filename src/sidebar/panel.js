import {getBrowserData, writeCookies} from "./browser.js";
import {detectPluginsForCurrentUrl, runPlugin, setWindowId} from "./client.js";
import {writeMetadata} from "./download.js";
import {
  attachElementHandler,
  createElement,
  form2json,
  generateElementUID,
  removeChildElements,
  removeElementsById,
} from "./html.js";

let currentHostname;

// Update the sidebar's content
const panelOptions = document.getElementById("js-options");
const panelOptionsStatus = document.getElementById("js-options-status");
const panelOptionsAction = document.getElementById("js-options-action");
const panelOptionsActionOpt = document.getElementById("js-options-action-opt");

const panelResults = document.getElementById("js-results");

async function refreshPanelOptions() {
  const [newHostname, plugins] = await detectPluginsForCurrentUrl();

  if (currentHostname === newHostname) {
    // Don't rebuild options unless there is a new host
    return;
  } else {
    // For a new host, clear previous plugins
    currentHostname = newHostname;
    removeChildElements(panelOptionsActionOpt);
  }


  if (plugins.length) {
    panelOptionsStatus.value = newHostname;
    panelOptionsStatus.dataset.validity = "valid";


    for (const plugin of plugins) {
      const optElement = createElement(
        "option",
        {
          "value": plugin.filename,
          "selected": true,
        },
      );

      optElement.innerText = plugin.name;

      panelOptionsActionOpt.appendChild(optElement);
    }
  } else {
    panelOptionsStatus.value = newHostname;
    panelOptionsStatus.dataset.validity = "indeterminate";

    panelOptionsAction.selectedIndex = -1;
  }
}

// Update content when a new tab becomes active
browser.tabs.onActivated.addListener(refreshPanelOptions);

// Update content when a new page is loaded into a tab
browser.tabs.onUpdated.addListener(refreshPanelOptions);

// When the sidebar loads, get the ID of its window, and update its content
browser.windows.getCurrent({populate: true})
  .then(windowInfo => {
    setWindowId(windowInfo.id);
    refreshPanelOptions();
  });

/*
 * Options Submit Handler
 */

panelOptions.addEventListener(
  "submit",
  async event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const action = formData.get("action");

    switch (action) {
      case "browser": {
        updateResultsBrowser(panelResults);
        break;
      }
      default: {
        const metadata = await runPlugin(action);

        updateResults(metadata[0], panelResults);
      }
    }
  },
  false,
);

panelResults.addEventListener(
  "submit",
  event => {
    event.preventDefault();
    const formData = new FormData(event.target);
    const action = formData.get("action");
    formData.delete("action");

    switch (action) {
      case "browser": {
        // NOTE: 'cookies.txt' MUST use tabs, and copy-pasting whitespace can
        // be fiddly when software tries to be "helpful".
        // This only downloads the cookies, it doesn't try to write out the UA.
        const cookies = formData.get("cookies");

        writeCookies(cookies);
        break;
      }
      default: {
        const formJson = form2json(formData);

        writeMetadata(formJson);
      }
    }
  },
  false,
);

async function updateResultsBrowser(outputEl) {
  // Empty the form first:
  removeChildElements(outputEl);

  const actionEl = createActionElement("browser");
  outputEl.appendChild(actionEl);

  const metadata = await getBrowserData();

  Object.keys(metadata).forEach(key => {
    const label = createLabelElement(key);
    const value = createElement(
      "textarea",
      {
        "name": key,
        "class": "l-form__wide meta-form__console",
        "spellcheck": false,
        "readonly": "readonly",
      },
    );

    value.textContent = metadata[key];

    outputEl.appendChild(label);
    outputEl.appendChild(value);
  });


  const downloadElement = createElement(
    "input",
    {
      "type": "submit",
      "class": "l-form__wide l-form__submit",
      "name": "download",
      "value": "Download",
    },
  );

  outputEl.appendChild(downloadElement);
}

/*
 * Download button and populate form
 */

function updateResults(metadata, outputEl) {
  // Empty the form first:
  removeChildElements(outputEl);

  const actionEl = createActionElement("kodiNFO");
  outputEl.appendChild(actionEl);

  Object.keys(metadata).forEach(key => {
    _buildForm(outputEl, key, metadata[key]);
  });

  const downloadElement = createElement(
    "input",
    {
      "type": "submit",
      "class": "l-form__wide l-form__submit",
      "name": "download",
      "value": "Download",
    },
  );

  // const resetElement = createElement(
  //   "input",
  //   {
  //     "type": "reset",
  //     "class": "l-form__reset",
  //     "name": "reset",
  //     "value": "Reset",
  //   },
  // );


  outputEl.appendChild(downloadElement);
  // outputEl.appendChild(resetElement);
}

/*
 * Form
 */

// The frontend supports displaying K:V, where V can be:
// * primitive,
// * array[primitive],
// * array[object[primitive]],
// * object[primitive]
// Any value that is an array can have it's elements added or removed.
// Anything more complicated doesn't currently have a defined UX so processing
// behaviour is undefined (probably `[object Object]`).
function _buildForm(parentNode, keyName, keyContents, index=-1) {
  if (index === -1) {
    // Top level, create the key
    const labelElement = createLabelElement(keyName);
    parentNode.appendChild(labelElement);
  }
  if (Array.isArray(keyContents)) {
    if(typeof keyContents[0] === "object") {
      // array of objects
      // Generate additional dynamic objects based on the first
      const skeleton = {};
      Object.keys(keyContents[0]).forEach(key => skeleton[key] = "");

      keyContents.forEach((value, idx) => {
        _buildForm(parentNode, keyName, value, idx);
      });

      // Note: Intentionally executing this to init the index iterator
      const addElementHandler =
        addDynamicMapElement(keyName, skeleton, keyContents.length);
      const additonalDynamicElementButton =
        createAdditonalDynamicElementButton(keyName, addElementHandler);
      parentNode.appendChild(additonalDynamicElementButton);
    } else {
      // array of primitive types
      keyContents.forEach(value => {
        _buildForm(parentNode, keyName, value, keyContents.length);
      });
      const addElementHandler = () => addDynamicArrayElement(keyName);
      const additonalDynamicElementButton =
        createAdditonalDynamicElementButton(keyName, addElementHandler);
      parentNode.appendChild(additonalDynamicElementButton);
    }
  } else if(typeof keyContents === "object") {
    // object of primitives
    const [fieldset, removeButtonElement] =
      createDynamicMapElement(keyName, keyContents, index);

    parentNode.appendChild(fieldset);
    if (index > -1) {
      parentNode.appendChild(removeButtonElement);
    }
  } else {
    // primitive
    if (index > -1) {
      // dynamic
      const [inputElement, removeButtonElement] =
        createDynamicElement(keyName, keyContents);

      parentNode.appendChild(inputElement);
      parentNode.appendChild(removeButtonElement);
    } else {
      const inputElement = createInputElement(keyName, keyContents);
      parentNode.appendChild(inputElement);
    }
  }
}

function createActionElement(action) {
  return createElement(
    "input",
    {
      "type": "hidden",
      "id": "action",
      "name": "action",
      "value": action,
    },
  );
}

/*
 * Form - Helper functions
 */

function addDynamicArrayElement(name) {
  const [inputElement, removeButtonElement] = createDynamicElement(name);

  const parentAddElement = document.getElementById(`js-add_${name}`);
  parentAddElement.form.insertBefore(inputElement, parentAddElement);
  parentAddElement.form.insertBefore(removeButtonElement, parentAddElement);
}

function addDynamicMapElement(name, map, startIdx) {
  // Using a closure to ensure each time this runs the index is incremented
  let idx = startIdx;

  return () => {
    const [inputElement, removeButtonElement] =
      createDynamicMapElement(name, map, idx);

    const parentAddElement = document.getElementById(`js-add_${name}`);
    parentAddElement.form.insertBefore(inputElement, parentAddElement);
    parentAddElement.form.insertBefore(removeButtonElement, parentAddElement);
    idx++;
  };
}

/*
 * Form - View builders
 */

function createLabelElement(name, text=null) {
  const labelElement = createElement(
    "label",
    {
      "for": name,
      "class": "l-form__key",
    },
  );

  labelElement.innerText = text ? text : name;

  return labelElement;
}

function createInputElement(name, value="", uid=null, type="text") {
  return createElement(
    "input",
    {
      "type": type,
      "class": "l-form__value",
      "id": uid ? `meta_${name}_${uid}` : `meta_${name}`,
      "name": name,
      "value": value,
      "spellcheck": true,
    },
  );
}

function createRemoveButtonElement(name, uid) {
  const el = createElement(
    "button",
    {
      "type": "button",
      "name": `remove_${name}`,
      "id": `js-remove_${name}_${uid}`,
      "class": "l-form__special meta-form__special",
    },
  );

  el.innerText = "-";

  attachElementHandler(el, () => removeElementsById(
    `meta_${name}_${uid}`, // input
    `js-remove_${name}_${uid}`, // button
  ));

  return el;
}

function createAdditonalDynamicElementButton(name, elementHandler) {
  const el = createElement(
    "button",
    {
      "type": "button",
      "name": `add_${name}`,
      "id": `js-add_${name}`,
      "class": "l-form__value",
    },
  );

  el.innerText = `Add ${name}`;

  attachElementHandler(el, elementHandler);

  return el;
}

function createDynamicElement(name, value="") {
  const uid = generateElementUID();

  const inputElement =
    createInputElement(`${name}[]`, value, uid);

  const removeButtonElement = createRemoveButtonElement(`${name}[]`, uid);

  return [inputElement, removeButtonElement];
}

function createDynamicMapElement(name, map, idx) {
  const uid = generateElementUID();

  const fieldset = createElement(
    "fieldset",
    {
      "id": `meta_${name}_${uid}`,
      "class": "l-form__value meta-form__dynamic-map",
    },
  );

  Object.keys(map).forEach(key => {
    let elementName = `${name}[${idx}][${key}]`;
    if (idx === -1) {
      // element is not indexable
      elementName = `${name}[${key}]`;
    }

    const labelElement =
      createLabelElement(elementName, key);
    fieldset.appendChild(labelElement);

    const inputElement =
      createInputElement(elementName, map[key]);
    fieldset.appendChild(inputElement);
  });

  const removeButtonElement = createRemoveButtonElement(name, uid);

  return [fieldset, removeButtonElement];
}
