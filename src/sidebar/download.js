function resolveFileExtensionFromUrl(url) {
  // This function has no support for double-extensions such as `.tar.gz`.
  const filepathParts = new URL(url).pathname.split("/").reverse();
  let basename = "";

  for (const part of filepathParts) {
    if (part) {
      basename = part;
      break;
    }
  }

  let extension = basename.split(".").pop();

  if (
    // No filename
    !extension

    // Filename without an extension
    || extension === basename

    // Some servers will cache-bust here creating weird extensions
    || extension.length > 4
  ) {
    extension = "unknown";
  }

  return extension;
}

function writeMetadata(metadata) {
  // Process and remove Metadata Grabber config which isn't part of the NFO spec
  const rootNode = metadata._root;
  delete metadata["_root"];

  const fileIdentifier = metadata._filename.replace(/[<>:"/|?!*]/g, "").trim();
  delete metadata["_filename"];

  /*
   * Kodi has two "patterns" for naming ".nfo" files:
   *
   * - "<filename>.nfo" where "<filename>" is consistent with all files.
   * - "<rootnode>.nfo" where "<rootnode>" matches the root XML node.
   *
   * Depending on the media type, one or both may be supported:
   *
   * - "Movie" - Supports both "<filename>.nfo" & "<rootnode>.nfo" ("movie.nfo")
   *             However, docs prefer "<filename>.nfo" as the default.
   * - "TV Show" - Only supports "<rootnode>.nfo" ("tvshow.nfo").
   * - "Episode" - Only supports "<filename>.nfo"
   *
   */
  const filenameInMetadata = metadata._filenameInMetadata.trim() === "true" ? true : false;
  delete metadata["_filenameInMetadata"];

  const prettyOut = xmlToString(jsonToNFOXML(metadata, rootNode));

  let filePrefix = "";

  if (filenameInMetadata) {
    filePrefix = `${fileIdentifier}-`;
  }

  browser.downloads.download({
    url: URL.createObjectURL(new Blob([ prettyOut ])),
    filename: filenameInMetadata
      ? `${fileIdentifier}/${fileIdentifier}.nfo`
      : `${fileIdentifier}/${rootNode}.nfo`,
    saveAs: false,
  });

  if (metadata.trailer) {
    const fileExtension = resolveFileExtensionFromUrl(metadata.trailer);
    browser.downloads.download({
      url: metadata.trailer,
      filename: `${fileIdentifier}/${filePrefix}trailer.${fileExtension}`,
      saveAs: false,
    });
  }

  if (metadata.thumb) {
    metadata.thumb.forEach(thumb => {
      if (thumb["#aspect"] && thumb["=value"]) {
        const fileExtension = resolveFileExtensionFromUrl(thumb["=value"]);
        browser.downloads.download({
          url: thumb["=value"],
          filename: `${fileIdentifier}/${filePrefix}${thumb["#aspect"]}.${fileExtension}`,
          saveAs: false,
        });
      }
    });
  }

  metadata.fanart?.forEach((imageUrl, i) => {
    if (imageUrl) {
      const fileExtension = resolveFileExtensionFromUrl(imageUrl);
      browser.downloads.download({
        url: imageUrl,
        filename: `${fileIdentifier}/${filePrefix}fanart${i ? i : ""}.${fileExtension}`,
        saveAs: false,
      });
    }
  });

  // Although this should be saved to `identifier/.actors/First_Last.ext`
  // Firefox (sensibly) doesn't let web-ext make hidden directories.
  metadata.actor?.forEach(actor => {
    if (actor?.thumb) {
      const fileExtension = resolveFileExtensionFromUrl(actor.thumb);
      browser.downloads.download({
        url: actor.thumb,
        filename: `${fileIdentifier}/actors/${actor.name.replace(" ", "_")}.${fileExtension}`,
        saveAs: false,
      });
    }
  });
}

function _sanitiseValue (value) {
  return `${value}`
    // Absolutely critical "&" goes first!
    .replaceAll("&", "&amp;")
    .replaceAll("<", "&lt;")
    .replaceAll(">", "&gt;")
    .replaceAll("\\n", "\n");
}

function _buildXML (primaryNode, parentNode, keyName, keyContents) {
  if (Array.isArray(keyContents)) {
    if (keyName === "fanart") {
      /* The layout of the fanart element is an exception - it looks like:
       *
       *  <fanart>
       *    <thumb>item 1</thumb>
       *    <thumb>item 2</thumb>
       *  </fanart>
       *
       */
      const nestedParent = primaryNode.createElement(keyName);
      keyContents.forEach(value => {
        _buildXML(primaryNode, nestedParent, "thumb", value);
      });
      parentNode.appendChild(nestedParent);
    } else {
      keyContents.forEach(value => {
        _buildXML(primaryNode, parentNode, keyName, value);
      });
    }
  } else if(typeof keyContents === "object") {
    const nestedParent = primaryNode.createElement(keyName);
    Object.keys(keyContents).forEach(key => {
      _buildXML(primaryNode, nestedParent, key, keyContents[key]);
    });
    parentNode.appendChild(nestedParent);
  } else {
    if (keyName[0] === "#") {
      // Attribute of parent
      parentNode.setAttribute(keyName.slice(1), keyContents);
    } else if (keyName === "=value") {
      // Value of parent
      parentNode.innerHTML = _sanitiseValue(keyContents);
    } else {
      // Child of parent
      const node = primaryNode.createElement(keyName);
      node.innerHTML = _sanitiseValue(keyContents);
      parentNode.appendChild(node);
    }
  }
}

function jsonToNFOXML(metadata, rootNode="movie") {
  const nfo = document.implementation.createDocument("", "", null);

  // Build and insert XML Declaration
  const declaration = nfo.createProcessingInstruction(
    "xml",
    // The current xmlwriter writes double-quotes and cannot inspect the
    // inside of a processing instruction to be consistent there.
    // eslint-disable-next-line quotes
    'version="1.0" encoding="UTF-8" standalone="yes"',
  );
  nfo.appendChild(declaration);

  _buildXML(nfo, nfo, rootNode, metadata);

  return nfo;
}

function xmlToString(xml) {
  const serialiser = new XMLSerializer();
  const xmlString = serialiser.serializeToString(xml);

  // Pretty print the XML
  // Plagiarised from https://stackoverflow.com/a/49458964
  let output = "", currentIndentation = "";
  const indentation = "  ";
  xmlString.split(/>\s*</).forEach(node => {
    if (node.match( /^\/\w/ )) {
      // decrease indent by one "level"
      currentIndentation = currentIndentation.substring(indentation.length);
    }
    output += `${currentIndentation}<${node}>\n`;
    if (node.match( /^<?\w[^>]*[^/]$/ )) {
      // increase indent by one "level"
      currentIndentation += indentation;
    }
  });
  return output.substring(1, output.length-2);
}

export {resolveFileExtensionFromUrl, writeMetadata};
