import {findJsonLD, htmlDecode, isoDurationToMinutes} from "./core/helpers.js";
import {movie} from "./core/kodiTemplates.js";

// METADATA_GRABBER_NAME = "IMDB";
// METADATA_GRABBER_URL = "www.imdb.com";
// METADATA_GRABBER_VERSION = "2022-05-23";

function scrapePage() {
  const metadata = movie();

  const jsonLD = findJsonLD();

  const title = htmlDecode(jsonLD.Movie.name);

  const releaseDate = new Date(jsonLD.Movie.datePublished);
  const releaseYYYY = releaseDate.getUTCFullYear();

  const imdb_id = jsonLD.Movie.url.split("/")[2];

  metadata._filename = `${title} (${releaseYYYY})`;
  metadata.title = title;
  metadata.plot = jsonLD.Movie.description;
  metadata.runtime = isoDurationToMinutes(jsonLD.Movie.duration);
  metadata.thumb = [
    {
      "#aspect": "poster",
      "=value": jsonLD.Movie.image,
    },
  ];
  metadata.mpaa = jsonLD.Movie.contentRating;
  metadata.id = imdb_id;
  metadata.uniqueid = [
    {
      "#type": "imdb",
      "=value": imdb_id,
    },
  ];
  metadata.genre = jsonLD.Movie.genre;
  metadata.tag = jsonLD.Movie.keywords.split(",");

  if (jsonLD.Movie.director) {
    metadata.director = jsonLD.Movie.director.map(director => {
      return director.name;
    });
  }

  metadata.premiered = jsonLD.Movie.datePublished;
  metadata.year = releaseYYYY;
  metadata.trailer = jsonLD.Movie.trailer.embedUrl;

  if (jsonLD.Movie.actor) {
    metadata.actor = jsonLD.Movie.actor.map(actor => {
      return {
        "name": actor.name,
        "role": "",
        "thumb": "",
      };
    });
  }

  return metadata;
}

scrapePage();
