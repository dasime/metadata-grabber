/**
 * @param {string} isoDuration - Duration as an ISO duration string (eg PT5M).
 * @returns {number} Total duration in minutes.
 */
function isoDurationToMinutes(isoDuration) {
  const isoDurationParseRegex =
    /PT(?:(?<hours>\d+)H)?(?:(?<minutes>\d+)M)?(?:(?<seconds>\d+(?:\.\d+)?)S)?/;

  return _durationStrToMinutes(isoDuration, isoDurationParseRegex);
}

/**
 * @param {string} timecode - Duration as a timecode string (eg 11:53).
 * @returns {number} Total duration in minutes.
 */
function timecodeToMinutes(timecode) {
  // Regex based on: https://stackoverflow.com/a/8318367
  const timecodeRegex =
    /^(?:(?:(?<hours>\d+):)?(?<minutes>[0-5]?\d):)?(?<seconds>[0-5]?\d)$/;

  return _durationStrToMinutes(timecode, timecodeRegex);
}

/**
 * @param {string} durationStr - Duration as a string (eg 11:53 or PT5M).
 * @param {regex} regexMask - Regex for parsing durationStr.
 *    Must use capture groups for 'hours', 'minutes' and 'seconds'.
 * @returns {number} Total duration in minutes.
 */
function _durationStrToMinutes(durationStr, regexMask) {
  const durationPieces = durationStr.match(regexMask);

  let duration = 0;

  if (durationPieces) {
    duration += parseInt(durationPieces.groups.hours || 0) * 60;
    duration += parseInt(durationPieces.groups.minutes || 0);
    duration += Math.round((durationPieces.groups.seconds || 0) / 60);
  }

  return duration;
}

// Decode HTML entities (like '&nbsp;')
// Optionally discard HTML tags (like '<div>')
function htmlDecode(htmlString, discardTags=false) {
  const textarea = document.createElement("textarea");
  textarea.innerHTML = discardTags
    ? htmlString.replace(/(<([^>]+)>)/gi, "")
    : htmlString;
  return textarea.value;
}

/**
 * @returns {Object} An object containing parsed JSON-LD records.
 */
function findJsonLD() {
  // There may be multiple JSON-LD scripts available
  // and each type will expose different amounts of data.
  const jsonLDAvailable =
    document.querySelectorAll("script[type='application/ld+json']");

  const jsonLD = {};

  jsonLDAvailable.forEach(jsonLDStr => {
    const jsonLDParsed = JSON.parse(jsonLDStr.textContent);
    jsonLD[jsonLDParsed["@type"]] = jsonLDParsed;
  });

  return jsonLD;
}

/**
 * @returns {string} The current date as "YYYY-mm-DD HH:MM:SS".
 */
function dateToday() {
  const now = new Date().toISOString().split("T");
  return `${now[0]} ${now[1].split(".")[0]}`;
}

/**
 * JavaScript's (in)famously weird date parser chokes on dates
 * that don't include a day and doesn't support custom parse strings.
 *
 * See tests for example supported strings.
 *
 * @param {string} dateString - en_US or en_GB verbose date string
 * @returns {date} Parsed date.
 */
function parseDateString(dateString) {
  const datePattern =
    /(?:(?<IntDay>\d{1,2}))?\s?(?<month>[a-zA-Z]+)\s?(?:(?<USDay>\d{1,2}))?,?\s(?<year>\d{4})/;
  const datePieces = dateString.match(datePattern);

  const dateComponents = [
    datePieces.groups.year,
    datePieces.groups.month,
    datePieces.groups.IntDay || datePieces.groups.USDay || 1,
    // Unless we specify a +0000 timezone JS will assume the local timezone
    // when creating the Date object and will then then convert to +0000
    // when calling 'Date.toISOString()' to print which can cause rounding
    "UTC",
  ];

  return new Date(dateComponents.join(" "));
}

/**
 * @param {function} substitutions - A function which returns a
 *                                   {key: "value"} mapping
 * @returns {function} A function which takes a string and returns either
 *                     it's normal form or itself
 */
function initSubstitution(substitutions) {
  const dataMapping = substitutions();

  return item => {
    const normalisedItem = item
      .replace(/\s+/g, " ") // No more than one space, turn NBSP, etc into space
      .replace(/[\p{Control}\p{Format}]+/gu, "") // Drop Control and Format characters
      .trim()
      .toLowerCase();

    // substitutions returns null for junk items
    return normalisedItem in dataMapping
      ? dataMapping[normalisedItem]
      : item;
  };
}

/**
 * Kinda like Document.querySelector() except for XPath Expressions
 * instead of CSS Selectors.
 *
 * @param {string} xpathExpression - An XPath Expression
 * @param {Node} contextNode - A Node to evaluate against (defaults to Document)
 * @returns {Node} A Node (or null) that matches the provided XPath Expression
 */
function getElementByXPath(xpathExpression, contextNode=null) {
  return document.evaluate(
    xpathExpression,
    contextNode || document,
    null, // namespaceResolver
    XPathResult.FIRST_ORDERED_NODE_TYPE, // resultType
    null, // result
  ).singleNodeValue;
}

/**
 * Firefox _XRAY VISION_! (Not available in Chrome)
 * Access page script object from content scripts using 'wrappedJSObject'
 * Docs: https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Sharing_objects_with_page_scripts
 *
 * @param {string} windowPropertyKey - Name of window property to access
 * @param {boolean} simpleClone - Simple sanitise of object (defaults to true)
 * @returns {Any} The contents of the requested window property
 */
function accessPageObject(windowPropertyKey, simpleClone=true) {
  const windowProperty = window.wrappedJSObject[windowPropertyKey];

  // NOTE: Using `JSON` to deep clone, because the source Object may be
  //    contaminated. We want a read-only copy as a simple Object.
  return simpleClone
    ? JSON.parse(JSON.stringify(windowProperty))
    : windowProperty;
}

/**
 * Utility for inverting name order, useful for localising names between
 * countries that order "GivenName FamilyName" and "FamilyName GivenName".
 *
 * Assumes names are only made up of:
 *  - Any kind of letter, from any language, based on it's Unicode property
 *    https://unicode.org/reports/tr18/#General_Category_Property
 *  - Hyphens (-)
 *  - Apostrophes (')
 *
 * Assumes that name tokens are delimited by a whitespace (\s)
 *
 * Known limitations:
 *  - Is unable to handle unhyphenated names which shouldn't be inverted.
 *    Such as: 'Abraham Van Helsing' -> 'Helsing Van Abraham'
 *  - Doesn't support non-latin names. This is largely intentional because
 *    it's only correct for a subset of transliterated names.
 *    Adding support for Hanzi and other writing systems that don't use spaces
 *    to delimit words would be significantly more complex.
 *
 * @param {string} nameStr - A string of one-or-more names
 * @returns {string} A mangled version of the original name list
 */
function invertNameOrder(nameStr) {
  return nameStr
    .match(/([-'\p{Letter}]+(\s[-'\p{Letter}]+)*)/gu)
    .map((e, i) => {
      let name = e.trim().split(" ");
      if (name.length > 1) {
        name.reverse();
      }
      name = name.join(" ");
      return (i === 0) ? name : `(${name})`;
    })
    .join(" ");
}

export {
  accessPageObject,
  dateToday,
  findJsonLD,
  getElementByXPath,
  htmlDecode,
  initSubstitution,
  invertNameOrder,
  isoDurationToMinutes,
  parseDateString,
  timecodeToMinutes,
};
