import {dateToday} from "./helpers.js";

// Source
// https://kodi.wiki/view/NFO_files/Movies
function movie() {
  return {
    "_root": "movie",
    "_filename": "",
    "_filenameInMetadata": "true",
    "title": "",
    "originaltitle": "",
    "sorttitle": "",
    "set": {
      "name": "",
      "overview": "",
    },
    "plot": "",
    "runtime": 0,
    "thumb": [
      {
        "#aspect": "poster",
        "=value": "",
      },
    ],
    "fanart": [""],
    "mpaa": "",
    "id": "",
    "uniqueid": [
      {
        "#type": "",
        "=value": "",
      },
    ],
    "genre": [""],
    "tag": [""],
    "director": [""],
    "premiered": "",
    "year": 0,
    "studio": [""],
    "trailer": "",
    "actor": [
      {
        "name": "",
        // "role": "",
        "thumb": "",
      },
    ],
    "dateadded": dateToday(),
  };
}

// Source
// https://kodi.wiki/view/NFO_files/TV_shows
function tvshow() {
  return {
    "_root": "tvshow",
    "_filename": "",
    "_filenameInMetadata": "false",
    "title": "",
    "originaltitle": "",
    "showtitle": "",
    "sorttitle": "",
    "season": "",
    "episode": "",
    "plot": "",
    "tagline": "",
    "thumb": [
      {
        "#aspect": "poster",
        "=value": "",
      },
    ],
    "fanart": [""],
    "mpaa": "",
    "id": "",
    "uniqueid": [
      {
        "#type": "",
        "=value": "",
      },
    ],
    "genre": [""],
    "tag": [""],
    "premiered": "",
    "year": 0,
    "studio": [""],
    "trailer": "",
    "actor": [
      {
        "name": "",
        "role": "",
        "thumb": "",
      },
    ],
    "dateadded": dateToday(),
  };
}

export {movie, tvshow};
