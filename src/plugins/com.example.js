import {movie} from "./core/kodiTemplates.js";

// METADATA_GRABBER_NAME = "example site";
// METADATA_GRABBER_URL = "example.com";
// METADATA_GRABBER_URL = "server.dasi.me";
// METADATA_GRABBER_VERSION = "2020-03-22";

function scrapePage() {
  const metadata = movie();

  metadata._filename = "Foobar (2020)";
  metadata.title = "Foobar";
  metadata.set = {
    "name": "The Foobar Universe",
    "overview": "Lorem ipsum",
  };
  metadata.plot = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
  metadata.runtime = 60;
  metadata.thumb = [
    {
      "#aspect": "poster",
      "=value": "https://example.com/asset/foobar-poster.jpg",
    },
  ];
  metadata.fanart = [
    "https://example.com/asset/foobar.jpg",
    "https://example.com/asset/foobar-1.jpg",
    "https://example.com/asset/foobar-2.jpg",
    "https://example.com/asset/foobar-3.jpg",
    "https://example.com/asset/foobar-4.jpg",
    "https://example.com/asset/foobar-5.jpg",
    "https://example.com/asset/foobar-6.jpg",
    "https://example.com/asset/foobar-7.jpg",
    "https://example.com/asset/foobar-8.jpg",
    "https://example.com/asset/foobar-9.jpg",
    "https://example.com/asset/foobar-10.jpg",
  ];
  metadata.mpaa = "U";
  metadata.id = "aa1234567";
  metadata.uniqueid = [
    {
      "#type": "example",
      "=value": "aa1234567",
    },
  ];
  metadata.genre = [
    "Action",
    "Adventure",
  ];
  metadata.director = ["Alice"];
  metadata.year = 2020;
  metadata.premiered = "2020-03-22";
  metadata.studio = ["Bob"];
  metadata.actor = [
    {
      "name": "Charlie",
    },
    {
      "name": "Dan",
    },
  ];

  return metadata;
}

scrapePage();
