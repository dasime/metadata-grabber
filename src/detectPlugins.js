import {readdir, readFile} from "node:fs/promises";
import path from "path";

const hostUrlRegex =
  /(?:\/\/|#)?\s*METADATA_GRABBER_(?<key>[A-Z]+)\s*=\s*['"`](?<value>.+)['"`];?/g;


async function detectPlugins(pluginsPath) {
  const detectedPlugins = [];

  const files = await readdir(pluginsPath);

  for (const file of files) {
    if (file.slice(-3) === ".js") {
      const supportedHosts = [];
      const pluginMetadata = {
        hostname: null,
        filename: file,
      };

      const pluginPath = path.join(pluginsPath, file);
      const plugin = await readFile(pluginPath, "utf8");
      const metadataMatches = plugin.matchAll(hostUrlRegex);

      for (const metadata of metadataMatches) {
        if (metadata.groups.key === "URL") {
          supportedHosts.push(metadata.groups.value);
        } else {
          pluginMetadata[metadata.groups.key.toLowerCase()] =
            metadata.groups.value;
        }
      }

      supportedHosts.forEach(host => {
        const metadata = structuredClone(pluginMetadata);
        metadata.hostname = host;
        detectedPlugins.push(metadata);
      });
    }
  }

  return detectedPlugins;
}

export {detectPlugins};
