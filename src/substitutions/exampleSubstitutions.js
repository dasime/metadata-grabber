export default function () {
  return {
    "4コマ漫画": "Comic", // lit. "4-koma manga" - Japanese for a type of parody comic
    "bonus": null,
    "cult film": "Cult Classic", // For films about actual cults, see: "Cult"
    "indie": "Independent",
    "sale": null, // Knowing something went on sale is not useful
    "メカ": "Mecha", // lit. "meka" - Japanese for a type of robot show
  };
}
