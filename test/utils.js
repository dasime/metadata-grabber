import virtual from "@rollup/plugin-virtual";
import {JSDOM} from "jsdom";
import {rollup} from "rollup";


// Resolve a script that uses ECMAScript Modules (ESM) `import`s and `export`s
//   and return one reconciled script
async function resolveESMScript(importFilePath) {
  return rollup({
    input: "entry",
    // Don't try and find unused code - it's likely none of it is used yet!
    treeshake: false,
    plugins: [
      virtual({
        entry: `import '${importFilePath}';`,
      }),
    ],
  })
    .then(bundle => bundle.generate({format: "esm"}))
    .then(({output}) => output[0].code);
}

// Helper functions that operate directly on the DOM are tested
// against a "real" DOM rather than stubbing document and window.
function prepareDOM(htmlString, scriptFile) {
  const {window} = (
    new JSDOM(
      `
      <!DOCTYPE html>
      <html>
      ${htmlString}
      </html>
      `,
      {runScripts: "dangerously"},
    )
  );

  // Execute helpers by inserting a <script> tag containing it.
  const scriptEl = window.document.createElement("script");
  scriptEl.textContent = scriptFile;
  window.document.head.appendChild(scriptEl);

  return window;
}

export {resolveESMScript, prepareDOM};
