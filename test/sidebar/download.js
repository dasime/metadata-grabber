import assert from "assert";

import {resolveFileExtensionFromUrl} from "../../src/sidebar/download.js";
import {prepareDOM, resolveESMScript} from "../utils.js";


const downloadFile = await resolveESMScript("./src/sidebar/download.js");

describe("sidebar/download", () => {

  describe("resolveFileExtensionFromUrl()", () => {
    const testCases = [
      {
        desc: "a plain HTTP URL at root",
        inputUrl: "http://example.com/foo.txt",
        expectedExt: "txt",
      },
      {
        desc: "a plain HTTPS URL at root",
        inputUrl: "https://example.com/foo.txt",
        expectedExt: "txt",
      },
      {
        desc: "a plain URL with a deep path",
        inputUrl: "http://example.com/many/levels/deep/foo.txt",
        expectedExt: "txt",
      },
      {
        desc: "a plain URL with multiple dots in the filename",
        inputUrl: "http://example.com/foo.bar.bam.tar.txt",
        expectedExt: "txt",
      },
      {
        desc: "a plain URL with a trailing slash",
        inputUrl: "http://example.com/foo.txt/",
        expectedExt: "txt",
      },
      {
        desc: "a plain URL with a trailing multiple slashes",
        inputUrl: "http://example.com/foo.txt///",
        expectedExt: "txt",
      },
      {
        desc: "a plain URL with a filename but no extension and multiple slashes",
        inputUrl: "http://example.com//foo//",
        expectedExt: "unknown",
      },
      {
        desc: "a plain URL without a filename or extension but multiple slashes",
        inputUrl: "http://example.com////",
        expectedExt: "unknown",
      },
      {
        desc: "a plain URL without an extension but a filename-ish path",
        inputUrl: "http://example.com/foo.txt/bar",
        expectedExt: "unknown",
      },
      {
        desc: "a plain URL without an extension",
        inputUrl: "http://example.com/foo",
        expectedExt: "unknown",
      },
      {
        desc: "a plain URL without an extension and a deep path",
        inputUrl: "http://example.com/many/levels/deep/foo",
        expectedExt: "unknown",
      },
      {
        desc: "a plain URL with an excessively long extension",
        inputUrl: "http://example.com/foo.12345",
        expectedExt: "unknown",
      },
      {
        desc: "a URL without a path",
        inputUrl: "http://example.com/",
        expectedExt: "unknown",
      },
      {
        desc: "a URL with query params",
        inputUrl: "http://example.com/foo.txt?key=value-a&key2=value.b",
        expectedExt: "txt",
      },
      {
        desc: "a URL with a fragment",
        inputUrl: "http://example.com/foo.txt#12-345.678",
        expectedExt: "txt",
      },
      {
        desc: "a URL with userinfo and a port",
        inputUrl: "http://username:password@example.com:8080/foo.txt",
        expectedExt: "txt",
      },
    ];

    testCases.forEach(({desc, inputUrl, expectedExt}) => {
      it(`should handle ${desc}`, () => {
        assert.equal(
          resolveFileExtensionFromUrl(inputUrl),
          expectedExt,
        );
      });
    });
  });

  // Comparing two XMLDocuments in Nodejs is messy due to the lack of
  // reasonable native implementations.
  // sidebar/download currently calls these two internal functions together
  // as one operation so this is fine until that changes.
  describe("xmlToString(jsonToNFOXML())", () => {
    const window = prepareDOM("", downloadFile);

    const testCases = [
      {
        desc: "empty JSON",
        inputJson: {},
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie/>`,
      },
      {
        desc: "simple K:V JSON",
        inputJson: {"string": "foo", "number": 0, "empty": ""},
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string>foo</string>
  <number>0</number>
  <empty/>
</movie>`,
      },
      {
        desc: "simple K:V JSON with CJK",
        inputJson: {"string": "汉语", "日本語": 0, "한국어": ""},
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string>汉语</string>
  <日本語>0</日本語>
  <한국어/>
</movie>`,
      },
      {
        desc: "JSON with an array containing simple values",
        inputJson: {"string": "foo", "array": ["1", 2, ""]},
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string>foo</string>
  <array>1</array>
  <array>2</array>
  <array/>
</movie>`,
      },
      {
        desc: "JSON with an array containing an object",
        inputJson: {
          "string": "foo",
          "array": [
            {"string": "bar", "number": 1, "empty": ""},
            {"string": "bam", "number": 2, "empty": ""},
          ],
        },
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string>foo</string>
  <array>
    <string>bar</string>
    <number>1</number>
    <empty/>
  </array>
  <array>
    <string>bam</string>
    <number>2</number>
    <empty/>
  </array>
</movie>`,
      },
      {
        desc: "JSON with an object",
        inputJson: {
          "string": "foo",
          "object": {"string": "bar", "number": 1, "empty": ""},
        },
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string>foo</string>
  <object>
    <string>bar</string>
    <number>1</number>
    <empty/>
  </object>
</movie>`,
      },
      {
        desc: "JSON with an object containing an array",
        inputJson: {
          "string": "foo",
          "object": {"array": ["1", 2, ""]},
        },
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string>foo</string>
  <object>
    <array>1</array>
    <array>2</array>
    <array/>
  </object>
</movie>`,
      },
      {
        desc: "JSON with an object containing an object",
        inputJson: {
          "string": "foo",
          "object": {
            "inner-object": {
              "string": "bar",
              "number": 1,
              "empty": "",
            },
          },
        },
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string>foo</string>
  <object>
    <inner-object>
      <string>bar</string>
      <number>1</number>
      <empty/>
    </inner-object>
  </object>
</movie>`,
      },
      {
        desc: "simple values with attributes",
        inputJson: {
          "string": {
            "=value": "foo",
            "#bar": "bam",
          },
          "number": {
            "#number": 1,
            "#string": "foo",
            "=value": 2,
          },
          "weird-double": {
            "#a1": "",
            "=value": 2,
            // Intentionally using an object with weird values
            // eslint-disable-next-line no-dupe-keys
            "=value": 3,
          },
        },
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string bar="bam">foo</string>
  <number number="1" string="foo">2</number>
  <weird-double a1="">3</weird-double>
</movie>`,
      },
      {
        desc: "objects with mixed attributes and children",
        inputJson: {
          "string": "foo",
          "object": {
            "#number": 1,
            "inner-object": {
              "#string": "bar",
              "number": 1,
              "empty": "",
              "#empty": "",
            },
          },
        },
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <string>foo</string>
  <object number="1">
    <inner-object string="bar" empty="">
      <number>1</number>
      <empty/>
    </inner-object>
  </object>
</movie>`,
      },
      {
        desc: "special characters to escaped values",
        inputJson: {
          "value": "<&><&>",
          "attribute": {
            "#single": "''",
            "#double": "\"\"",
          },
        },
        expectedXML:
`<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<movie>
  <value>&lt;&amp;&gt;&lt;&amp;&gt;</value>
  <attribute single="''" double="&quot;&quot;"/>
</movie>`,
      },
    ];

    testCases.forEach(({desc, inputJson, expectedXML}) => {
      it(`should convert ${desc}`, () => {
        assert.equal(
          window.xmlToString(window.jsonToNFOXML(inputJson)),
          expectedXML,
        );
      });
    });
  });

});
