import assert from "assert";

import {prepareDOM, resolveESMScript} from "../utils.js";


const htmlFile = await resolveESMScript("./src/sidebar/html.js");

describe("sidebar/html", () => {

  describe("createElement()", () => {
    const window = prepareDOM("", htmlFile);

    const testCases = [
      {
        desc: "create element without attributes (default attributes)",
        inputArgs: [
          "div",
        ],
        expectedHtml: "<div></div>",
      },
      {
        desc: "create element without attributes (empty attributes)",
        inputArgs: [
          "div",
          {},
        ],
        expectedHtml: "<div></div>",
      },
      {
        desc: "create element with attributes",
        inputArgs: [
          "div",
          {
            "id": "foo",
            "class": "bar",
          },
        ],
        // eslint-disable-next-line quotes
        expectedHtml: '<div id="foo" class="bar"></div>',
      },
    ];

    testCases.forEach(({desc, inputArgs, expectedHtml}) => {
      it(`should ${desc}`, () => {
        assert.equal(
          window.createElement(...inputArgs).outerHTML,
          expectedHtml,
        );
      });
    });
  });

  describe("generateElementUID()", () => {
    const window = prepareDOM("", htmlFile);

    const elementUIDs = [];
    let i = 0;
    const count = 50;

    while (i < count) {
      elementUIDs.push(window.generateElementUID());
      i++;
    }

    it(`should have generated ${count} unique ids`, () => {
      assert.equal(
        new Set(elementUIDs).size,
        count,
      );
    });

    it(`should have generated ${count} ids 8 characters long`, () => {
      assert.equal(
        elementUIDs.filter(u => u.length === 8).length,
        count,
      );
    });
  });

  describe("removeChildElements()", () => {
    const window = prepareDOM("", htmlFile);

    const testCases = [
      {
        desc: "remove nothing when there is nothing to remove",
        childHtml: "",
      },
      {
        desc: "remove multiple children",
        childHtml: "<h1>foo</h1><p>bar</p>",
      },
      {
        desc: "remove multiple children with nested children",
        childHtml: "<h1>foo</h1><p>bar <i>bam</i></p>",
      },
    ];

    testCases.forEach(({desc, childHtml}) => {
      it(`should ${desc}`, () => {
        const parentElement = window.document.createElement("div");
        parentElement.innerHTML = childHtml;

        assert.equal(
          parentElement.outerHTML,
          `<div>${childHtml}</div>`,
        );

        window.removeChildElements(parentElement);

        assert.equal(
          parentElement.outerHTML,
          "<div></div>",
        );
      });
    });
  });

  describe("removeElementsById()", () => {

    const testCases = [
      {
        desc: "remove nothing when there is nothing to remove",
        args: [],
        // eslint-disable-next-line quotes
        inputHtml: '<h1 id="123">foo</h1><p>bar</p>',
        // eslint-disable-next-line quotes
        expectedHtml: '<h1 id="123">foo</h1><p>bar</p>',
      },
      {
        desc: "remove one element",
        args: ["123"],
        // eslint-disable-next-line quotes
        inputHtml: '<h1 id="123">foo</h1><p>bar</p>',
        expectedHtml: "<p>bar</p>",
      },
      {
        desc: "remove multiple elements",
        args: ["123", "456"],
        // eslint-disable-next-line quotes
        inputHtml: '<h1 id="123">foo</h1><p>bar<i id="456">bam</i></p>',
        expectedHtml: "<p>bar</p>",
      },
    ];

    testCases.forEach(({desc, args, inputHtml, expectedHtml}) => {
      it(`should ${desc}`, () => {
        const window = prepareDOM(inputHtml, htmlFile);

        assert.equal(
          window.document.body.outerHTML.replace(/\s{2,}/g, ""),
          `<body>${inputHtml}</body>`,
        );

        window.removeElementsById(...args);

        assert.equal(
          window.document.body.outerHTML.replace(/\s{2,}/g, ""),
          `<body>${expectedHtml}</body>`,
        );
      });
    });
  });
});
