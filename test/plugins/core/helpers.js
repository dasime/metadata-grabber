import assert from "assert";

import {expect} from "chai";

import {
  initSubstitution,
  invertNameOrder,
  isoDurationToMinutes,
  parseDateString,
  timecodeToMinutes,
} from "../../../src/plugins/core/helpers.js";
import {prepareDOM, resolveESMScript} from "../../utils.js";


const helpersFile = await resolveESMScript("./src/plugins/core/helpers.js");

describe("plugins/_helpers", () => {

  describe("isoDurationToMinutes()", () => {
    const testCases = [
      {iso: "PT100H100M100S", minutes: 6102}, // supported but not sure if actually valid
      {iso: "PT100H59M59S", minutes: 6060},
      {iso: "PT1H1M1S", minutes: 61},
      {iso: "PT1H0M1S", minutes: 60},
      {iso: "PT1H1S", minutes: 60},
      {iso: "PT1H0S", minutes: 60},
      {iso: "PT1H", minutes: 60},
      {iso: "PT0H1M1S", minutes: 1},
      {iso: "PT1M1S", minutes: 1},
      {iso: "PT1M0S", minutes: 1},
      {iso: "PT1M", minutes: 1},
      {iso: "PT1S", minutes: 0},
      {iso: "PT0S", minutes: 0},
      {iso: "PT", minutes: 0},
      {iso: "", minutes: 0},
      {iso: "PT29S", minutes: 0},
      {iso: "PT30S", minutes: 1},
      {iso: "PT89S", minutes: 1}, // supported but not sure if actually valid
      {iso: "PT90S", minutes: 2}, // supported but not sure if actually valid
      {iso: "bad data", minutes: 0},
    ];

    testCases.forEach(({iso, minutes}) => {
      it(`should return ${minutes} minutes for ${iso} duration`, () => {
        assert.equal(isoDurationToMinutes(iso), minutes);
      });
    });
  });

  describe("timecodeToMinutes()", () => {
    const testCases = [
      {timecode: "100:59:59", minutes: 6060},
      {timecode: "1:1:1", minutes: 61},
      {timecode: "1:1:0", minutes: 61},
      {timecode: "1:0:1", minutes: 60},
      {timecode: "1:0:0", minutes: 60},
      {timecode: "0:0:0", minutes: 0},
      {timecode: "0:1:1", minutes: 1},
      {timecode:   "1:1", minutes: 1},
      {timecode:   "1:0", minutes: 1},
      {timecode:     "1", minutes: 0},
      {timecode:     "0", minutes: 0},
      {timecode:      "", minutes: 0},
      {timecode:    "29", minutes: 0},
      {timecode:  "1:29", minutes: 1},
      {timecode:    "30", minutes: 1},
      {timecode:  "1:30", minutes: 2},
      {timecode: "bad data", minutes: 0},
    ];

    testCases.forEach(({timecode, minutes}) => {
      it(`should return ${minutes} minutes for ${timecode} timecode`, () => {
        assert.equal(timecodeToMinutes(timecode), minutes);
      });
    });
  });

  describe("findJsonLD()", () => {
    const jsonLD1 = {
      "@context":"https://schema.org",
      "@type":"Article",
    };
    const jsonLD2 = {
      "@context":"https://schema.org",
      "@type":"Movie",
    };

    it("should return 0 linked data schemas", () => {
      const window = prepareDOM("", helpersFile);

      expect(window.findJsonLD()).deep.to.equal({});
    });

    it("should return 1 linked data schemas", () => {
      const window = prepareDOM(`
        <script type="application/ld+json">
          ${JSON.stringify(jsonLD1)}
        </script>
      `,
      helpersFile);

      expect(window.findJsonLD()).deep.to.equal({
        "Article": jsonLD1,
      });
    });

    it("should return 2 linked data schemas", () => {
      const window = prepareDOM(`
        <script type="application/ld+json">
          ${JSON.stringify(jsonLD1)}
        </script>
        <script type="application/ld+json">
          ${JSON.stringify(jsonLD2)}
        </script>
      `,
      helpersFile);

      expect(window.findJsonLD()).deep.to.equal({
        "Article": jsonLD1,
        "Movie": jsonLD2,
      });
    });
  });

  describe("initSubstitution()", () => {
    const testCases = [
      {
        desc: "empty substitutions",
        list: ["foo", "bar"],
        substitutions: () => ({}),
        expectedOutput: ["foo", "bar"],
      },
      {
        desc: "substitutions with no match",
        list: ["Foo", "bar"],
        substitutions: () => ({"bam": null}),
        expectedOutput: ["Foo", "bar"],
      },
      {
        desc: "substitutions with match (string)",
        list: ["Foo", "bar"],
        substitutions: () => ({"foo": "Bam"}),
        expectedOutput: ["Bam", "bar"],
      },
      {
        desc: "substitutions with match (null)",
        list: ["Foo", "bar"],
        substitutions: () => ({"foo": null}),
        expectedOutput: [null, "bar"],
      },
      {
        desc: "substitutions with match (CJK)",
        list: ["Foo", "汉语"],
        substitutions: () => ({"汉语": "日本語"}),
        expectedOutput: ["Foo", "日本語"],
      },
      {
        desc: "substitutions with extra whitespace",
        list: ["Foo    bar", " bar\u00A0NBSP"],
        substitutions: () => ({"foo bar": "Alpha", "bar nbsp": "Beta"}),
        expectedOutput: ["Alpha", "Beta"],
      },
      {
        desc: "substitutions with control or format characters",
        list: ["LTR \u200EMARK", "\uFEFFByte Order Mark", "NUL\u0000 Line Feed\u000A"],
        substitutions: () => ({"ltr mark": "LRM", "byte order mark": "BOM", "nul line feed": "NUL+LF"}),
        expectedOutput: ["LRM", "BOM", "NUL+LF"],
      },
    ];

    testCases.forEach(({desc, list, substitutions, expectedOutput}) => {
      it(`should return with ${desc}`, () => {
        const normalise = initSubstitution(substitutions);

        const output = list.map(item => normalise(item));

        expect(output).deep.to.equal(expectedOutput);
      });
    });
  });

  describe("parseDateString()", () => {
    const testCases = [
      {
        dateStr: "1 January 2000",
        dateObj: new Date("2000-01-01T00:00:00Z"),
      },
      {
        dateStr: "January 2000",
        dateObj: new Date("2000-01-01T00:00:00Z"),
      },
      {
        dateStr: "31 December 2000",
        dateObj: new Date("2000-12-31T00:00:00Z"),
      },
      {
        dateStr: "January 1, 2000",
        dateObj: new Date("2000-01-01T00:00:00Z"),
      },
      {
        dateStr: "January, 2000",
        dateObj: new Date("2000-01-01T00:00:00Z"),
      },
      {
        dateStr: "December 31, 2000",
        dateObj: new Date("2000-12-31T00:00:00Z"),
      },
    ];

    testCases.forEach(({dateStr, dateObj}) => {
      it(`should return '${dateObj.toISOString()}' for '${dateStr}'`, () => {
        expect(parseDateString(dateStr)).deep.to.equal(dateObj);
      });
    });
  });

  describe("getElementByXPath()", () => {
    const window = prepareDOM("<div><b>foo</b></div>", helpersFile);

    it("should resolve simple XPath Expressions", () => {
      assert.equal(
        window.getElementByXPath("//div/b").textContent,
        "foo",
      );
    });

    it("should resolve simple XPath Expressions w/ processing", () => {
      // The textContent attribute works fine on a Text Node (below), however
      // the nodeValue attribute tested here would equal 'null' on a HTML Node
      assert.equal(
        window.getElementByXPath("//div/b/text()").nodeValue,
        "foo",
      );
    });

    it("should return null for no matches", () => {
      assert.equal(
        window.getElementByXPath("//div/a"),
        null,
      );
    });
  });

  describe("htmlDecode()", () => {
    const window = prepareDOM("", helpersFile);

    const testCases = [
      {
        encodedHTML: "foo &amp; bar",
        discardTags: undefined,
        decodedHTML: "foo & bar",
      },
      {
        encodedHTML: "foo &nbsp; bar",
        discardTags: undefined,
        decodedHTML: "foo \u00A0 bar",
      },
      {
        encodedHTML: "foo&#39;s bar",
        discardTags: undefined,
        decodedHTML: "foo's bar",
      },
      {
        encodedHTML: "<p><i><div></div><span>foo&#39;s bar</span>",
        discardTags: null,
        decodedHTML: "<p><i><div></div><span>foo's bar</span>",
      },
      {
        encodedHTML: "<p><i><div></div><span>foo&#39;s bar</span>",
        discardTags: false,
        decodedHTML: "<p><i><div></div><span>foo's bar</span>",
      },
      {
        encodedHTML: "<p><i></h1><div></div><span>foo&#39;s bar</span>",
        discardTags: true,
        decodedHTML: "foo's bar",
      },
      {
        encodedHTML: "f<p>o<i class='12'>o<div> </div>&amp;<span id='ab'> b</h1>a</span>r",
        discardTags: true,
        decodedHTML: "foo & bar",
      },
    ];

    testCases.forEach(({encodedHTML, discardTags, decodedHTML}) => {
      it(
        `should return '${decodedHTML}' for '${encodedHTML} (${discardTags ? "discard" : "ignore"} tags)'`,
        () => {
          assert.equal(
            window.htmlDecode(encodedHTML, discardTags),
            decodedHTML,
          );
        },
      );
    });
  });

  describe("invertNameOrder()", () => {
    const testCases = [
      {
        inputName: "Alice",
        outputName: "Alice",
      },
      {
        inputName: "Alice Alpha",
        outputName: "Alpha Alice",
      },
      {
        inputName: "Alice Middle Alpha",
        outputName: "Alpha Middle Alice",
      },
      {
        inputName: "Alice Alpha (Bob Beta)",
        outputName: "Alpha Alice (Beta Bob)",
      },
      {
        inputName: "Alice Alpha (Bob Beta) (Carol)",
        outputName: "Alpha Alice (Beta Bob) (Carol)",
      },
      {
        inputName: "Alice Alpha-Delta (Bob Beta-Delta)",
        outputName: "Alpha-Delta Alice (Beta-Delta Bob)",
      },
      {
        inputName: "Alice O'Alpha (O'Bob Beta)",
        outputName: "O'Alpha Alice (Beta O'Bob)",
      },
      {
        inputName: "Alice Alpha; Bob Beta",
        outputName: "Alpha Alice (Beta Bob)",
      },
      {
        inputName: "Alice Alpha [Bob Beta]",
        outputName: "Alpha Alice (Beta Bob)",
      },
      {
        inputName: "Ãłîçé Åĺƥħà (Ɓŏƀ ʙæţā) (Ċǟřőƚ)",
        outputName: "Åĺƥħà Ãłîçé (ʙæţā Ɓŏƀ) (Ċǟřőƚ)",
      },
    ];

    testCases.forEach(({inputName, outputName}) => {
      it(`should invert '${inputName}'`, () => {
        assert.equal(
          invertNameOrder(inputName),
          outputName,
        );
      });
    });
  });


});
