# Evading Scraping Detection

Anybody who has previously maintained web scrapers will tell you it's
a tedious and often frustrating back-and-forth between the target webpage
unexpectedly updating, and having to rewrite the scraper to support those changes.

Over the past few years bot detection has reached the point where
it's _effectively_ impossible to use automated headless scraping on a
website that actively doesn't want you there.

That's when I came up with the idea to do the scraping from an active browser
session. This legitimately passes CloudFlare, it correctly renders JavaScript
and CSS, it provides a complete regular `window`, `document` and DOM.
Any unexpected captchas or surprise modals are handled up front without
code changes or reverse engineering.

That's the majority of suffering avoided. A-B testing not withstanding.
The modern browser extension API is now complete enough to replace most
of the previous NPAPI, XUL/XPCOM functionality needed for this without
having to call out to a native programs
(although the `download` API is still missing several useful features).

## Defending Extensions

Despite browser extensions being significantly harder to block than the usual
suspects of Selenium or Puppeteer, if done incorrectly they can still be caught.

[Research into how LinkedIn detects extensions](https://github.com/dandrews/nefarious-linkedin)
has highlighted several weaknesses.

### Avoid Web Accessible Resources

> _Sometimes you want to package resources - for example, images,_
> _HTML, CSS, or JavaScript - with your extension and make them_
> _available to web pages and other extensions._
>
> &mdash;[MDN](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/manifest.json/web_accessible_resources)

Having a unique file permanently accessible to the current webpage
gives that webpage an easy target for identifying the extension.

Metadata Grabber avoids this by not exposing any publicly accessible files.
Instead, JavaScript is only sent to the webpage as a
[content script](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Content_scripts)
when it's required.

### Avoid Modifying the Page

Creating your own elements, removing or otherwise mutating existing elements
are all easily detectable DOM events.

Metadata Grabber reports data extracted from the page in it's sidebar.
This avoids the need to inject any new elements into the page's DOM.

Content scripts get their own JavaScript scope, so a script from the
target page couldn't (for example), redefine a native JavaScript function
to make it crash or include a hook to spy on it's usage.

Based on this, functions that update the view without changing the page
such as `Element.scrollIntoView()` and `HTMLElement.click()` should be safe.
This is useful for forcing pages which lazy load content to go and load it.

It's not clear if browser actions like `window.prompt()` could be detected.
They don't have a presence in the DOM but they do block _some_ rendering.

It's even less clear if network requests created from using `fetch()` could be
detected. Either through CORS configuration, or bespoke server-side monitoring.

Where possible, Metadata Grabber plugins should try and treat the target
webpage as read-only. No other JavaScript features are currently restricted.
