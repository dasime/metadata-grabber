# `web-ext` Setup

To use custom configuration with `web-ext` create `./web-ext-config.mjs`
at the root of the project with your overrides. For example:

```js
/* eslint-env node */
import {existsSync, mkdirSync} from "node:fs";

process.env.TMPDIR = `${process.env.XDG_RUNTIME_DIR}/web-ext`;

if (!existsSync(process.env.TMPDIR)){
  mkdirSync(process.env.TMPDIR);
}

export default {
  run: {
    firefox: "./firefox-launch.sh",
  },
};
```

For more documentation on the config format see:
<https://extensionworkshop.com/documentation/develop/getting-started-with-web-ext/>

## Required Filesystem Access

_Most people run Firefox without additional sandboxing so this isn't relevant to most people._

If Firefox is run from a container then additional directories will need to be mounted.

### Temporary Profile Directory

By default `web-ext` creates temporary profiles in `/tmp` which can be
overridden by setting the environment variable `TMPDIR`.

Rather than allowing Firefox to access `/tmp` you can create a directory
specifically for `web-ext` such as `${XDG_RUNTIME_DIR}/web-ext`.

This env var can be added to the top of the `./web-ext-config.mjs` file like this:

```js
process.env.TMPDIR = `${process.env.XDG_RUNTIME_DIR}/web-ext`;
```

**NOTE:** `web-ext` will not attempt to create any directories,
so you will need to either run `mkdir "${XDG_RUNTIME_DIR}/web-ext"`
to create it or run `mkdir` from `web-ext-config.mjs`.

### Source Directory

Additionally, Firefox will need access to the project directory.
If the project is at `~/Documents/metadata-grabber` that's the path to mount
on your Firefox container.

## Flatpak Setup

`web-ext` struggles to launch nicely with the regular `flatpak` flow.
The easiest way is to point directly to the bin exported by Flatpak.

If you installed Firefox for `--user` rather than `--system` that'll look like:

```js
run: {
  firefox: `${process.env.XDG_DATA_HOME}/flatpak/exports/bin/org.mozilla.firefox`,
}
```

Additionally, the temporary profile and source directory need to be added
to the host paths mounted in Flatpak. See _Required Filesystem Access_ above.

The easiest way to add custom host filesystem access is using Flatseal.

## Other Launcher Setup

To wrap other unusual Firefox launchers you can write a short shell script:

```shell
#!/bin/bash

/usr/bin/flatpak run \
  --branch=stable \
  --arch=x86_64 \
  --command=firefox \
  --runtime=org.gnome.Platform//43 \
  --file-forwarding \
  --die-with-parent \
  org.mozilla.firefox $@
```

**NOTE:** The `$@` after `org.mozilla.firefox` which inserts all `web-ext` generated args.

Then just point `web-ext` to your shell script with `./web-ext-config.mjs` like:

```js
run: {
  firefox: "./firefox-launch.sh",
}
```
