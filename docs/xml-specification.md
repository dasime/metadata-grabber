# XML Specification

One part of the XML standard that JSON doesn't really have an
equivalent of is `attributes`.
In XML you can both nest other key-value pairs (like in JSON)
AND attach additional values directly to keys.

To support this, Metadata Grabber introduces two custom syntax identifiers:

* A key that starts with `#` means: _attribute of parent_
* A key of exactly `=value` means: _value of parent_

The `=value` key will clobber whatever value is already set in the parent.

For example:

```json
{
  "foo": {
    "#attr": "bar",
    "=value": "bam"
  }
}
```

Will evaluate to:

```xml
<foo attr="bar">bam</foo>
```

This syntax doesn't conflict with existing objects:

```json
{
  "foo": {
    "#attr": "bar",
    "bam": 1,
    "empty": ""
  }
}
```

Will evaluate to:

```xml
<foo attr="bar">
  <bam>1</bam>
  <empty/>
</foo>
```

The special characters `#` and `=` were chosen as identifiers for two reasons:

* Both are invalid `NameStartChar` so cannot be used to start keys in XML.
* In the URI standard, `#` is used to represent a fragment,
  which in a webpage can be used as a bookmark of an `id` attribute.
  So there is already precedent for `#` referencing an attribute.
* In the URI standard, `=` is used to represent the value of a query component.

## Specification Notes

### Special Characters

As with any language, usage of some special characters is restricted to avoid
conflict with the language parser.

> The ampersand character (`&`) and the left angle bracket (`<`) MUST NOT
> appear in their literal form, except when used as markup delimiters,
> or within a comment, a processing instruction, or a CDATA section.
>
> If they are needed elsewhere, they MUST be escaped using either
> numeric character references or the strings `&amp;` and `&lt;` respectively.
>
> The right angle bracket (`>`) may be represented using the string `&gt;`,
> and MUST, for compatibility, be escaped using either `&gt;` or a character
> reference when it appears in the string `]]>` in content,
> when that string is not marking the end of a CDATA section.

Continued

> To allow attribute values to contain both single and double quotes,
> the apostrophe or single-quote character (`'`) may be represented as `&apos;`,
> and the double-quote character (`"`) as `&quot;`.

This only covers the characters most likely to introduce parser issues.
There are several ranges of characters that are also restricted, but these
are less likely to be accidentally introduced to otherwise valid strings.

In short:

| Symbol (name)                  | Escape Sequence     | Where?    |
| ---                            | ---                 | ---       |
| < (less-than)                  | `&#60;` or `&lt;`   | value     |
| > (greater-than)               | `&#62;` or `&gt;`   | value     |
| & (ampersand)                  | `&#38;` or `&amp;`  | value     |
| ' (apostrophe or single quote) | `&#39;` or `&apos;` | attribute |
| " (double-quote)               | `&#34;` or `&quot;` | attribute |

Source:

* <https://www.w3.org/TR/2008/REC-xml-20081126/#syntax>
* <https://docs.oracle.com/cd/A97335_02/apps.102/bc4j/developing_bc_projects/obcCustomXml.htm>

### Attributes

Attributes must only start with a `NameStartChar`:

* `":"`
* `[A-Z]`
* `"_"`
* `[a-z]`
* `[#xC0-#xD6]`
* `[#xD8-#xF6]`
* `[#xF8-#x2FF]`
* `[#x370-#x37D]`
* `[#x37F-#x1FFF]`
* `[#x200C-#x200D]`
* `[#x2070-#x218F]`
* `[#x2C00-#x2FEF]`
* `[#x3001-#xD7FF]`
* `[#xF900-#xFDCF]`
* `[#xFDF0-#xFFFD]`
* `[#x10000-#xEFFFF]`

One omission of note is numbers.

Source:

* <https://www.w3.org/TR/2008/REC-xml-20081126/#sec-common-syn>
