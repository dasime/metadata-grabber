# <img src="/icons/noun_Tag_47019.svg" width="50"/> Metadata Grabber

The internet is ephemeral and saving things without also collecting the metadata
is asking for it to slip through your fingers.

This browser extension will pull metadata from the current page and
save it to [Kodi NFO format](https://kodi.wiki/view/NFO_files).

For more discussion on _why_ a browser extension and how to write hardened
plugins see: [Evading Scraping Detection](/docs/evading-scraping-detection.md).

## Installation

As this extension isn't published in the app store it must be installed manually.

To run the extension from source just run `npm start`.
This will handle creating a temporary profile and launching Firefox.

To install the extension permanently you need to use either
[Firefox ESR](https://www.mozilla.org/en-GB/firefox/enterprise/)
or [Firefox Developer Edition](https://www.mozilla.org/en-GB/firefox/developer/)
and disable `xpinstall.signatures.required` in `about:config`.

To install the extension temporarily in an existing regular Firefox profile:

> * Open Firefox
> * Enter `about:debugging` in the URL bar
> * Click _This Firefox_
> * Click _Load Temporary Add-on_
> * Open the extension's directory and select any file inside the extension,
>   or select the packaged extension (.zip file).

For more information on temporary installations see:
<https://extensionworkshop.com/documentation/develop/temporary-installation-in-firefox/>

Because the extension uses the `sidebarAction` API it is currently
[not compatible with Google Chrome](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/sidebarAction#browser_compatibility).

## Usage

![Animated demonstration](/docs/demo.gif)  
_A clip of fetching metadata from the IMDB page for Top Gun (1986)_

With the extension installed, use `CTRL+SHIFT+L` to force out the sidebar,
or open any other sidebar and navigate to _Metadata_.

With the sidebar extended, visit the webpage you want to collect metadata from.

If there is a plugin registered for that domain, the status indicator will
turn green and the _Run_ button will be enabled.

After pushing _Run_, a form will populate with all the data the
corresponding plugin managed to find on the page.
This form can be manually edited before pushing _Download_ at the bottom.

After pushing _Download_, the extension will attempt to create an appropriately
named directory, write the NFO, as well as downloading and naming correctly
any additional files such as trailers, posters, fanart and actor thumbnails.

## Advanced Usage

There are two ways to extend Metadata Grabber:

* Plugins - Scripts that extract information from a webpage
* Substitutions - Custom Key-Value lookups for normalising taxonomies

Substitutions are an entirely optional way to automate the very tedious job
of maintaining canonical taxonomies.
If that doesn't sound useful in your personal workflow
then substitutions can be completely ignored.

### Creating Plugins

Plugins are easy to create, just create a file in `./src/plugins`.
It's RECOMMENDED you follow reverse dot-notation of the site's address.

For example, a plugin for `https://imdb.com` is available at `./src/plugins/com.imdb.js`.

The plugin MUST register itself with a comment somewhere in the file like:

```js
// METADATA_GRABBER_NAME = "IMDB";
// METADATA_GRABBER_URL = "www.imdb.com";
// METADATA_GRABBER_VERSION = "2022-05-23";
```

It's then up to you to write the logic to scrape and `fetch` appropriate metadata.

Once you've gathered everything, the plugin MUST `return` it as JSON.

If you've got the web extension open with `npm start`, it will hot-reload as
you iterate your plugin.

### Creating Substitutions

Example use cases:

* Removing common junk tags (such as `Sale` or `Bonus`).

  ```js
  return {
    "bonus": null,
    "sale": null,
  }
  ```

* Normalising genres (such as `Indie` to `Independent`).

  ```js
  return {
    "cult film": "Cult Classic", // Explaining logic behind tagging preferences
    "indie": "Independent",
  }
  ```

* Basic translation for when you have a preferred tag (such as `メカ` to `Mecha`).

  ```js
  return {
    "4コマ漫画": "Comic", // lit. "4-koma manga" - Japanese for a type of parody comic
    "メカ": "Mecha", // lit. "meka" - Japanese for a type of robot show
  }
  ```

Substitutions are less complicated than plugins but must be imported and
initialised in a plugin before they are applied.

Substitutions MUST be created in `./src/substitutions`.

An example exists at: `./src/substitutions/exampleSubstitutions.js`.

The file MUST export a factory method which returns one-dimensional JSON.

The `key` MUST be lowercase, to reduce redundancy. All lookups are in lowercase.

The `value` MAY be any case or language appropriate for your taxonomy.

If the `value` is `null` that `key` will be removed.

It is RECOMMENDED you sort your substitutions alphabetically to make them easier
to maintain, but this is not required.

It is RECOMMENDED you use end-of-line comments where required to document the logic
behind substitutions.
EoL comments will not be separated if the substitutions are later automatically sorted.

If you've got the web extension open with `npm start`, it will hot-reload as
you iterate your substitutions.

## Permissions

This browser extension makes use of the following permissions.

### `<all_urls>`

This allows the extension to be activated on any hostname,
and to access any hostname when active.

In the future it might be possible to configure a more narrowly defined
list of URLs while building the extension.

### `activeTab`

This permission allows the extension to read data like the `url` and `title`
of the current tab.

This permission also allows the extension to execute scripts on the current tab,
which is used for running plugins.

It is only granted when the extension is active and only for the current tab.

### `downloads`

This permission allows the extension to start downloads with
the browser's download manager.

## Icon License

Icons in `./icons` (c) by artworkbean

* <https://thenounproject.com/artworkbean/>
* <https://dribbble.com/artworkbean>
* <https://www.fiverr.com/artworkbean>

Provided by [The Noun Project](https://thenounproject.com/icon/tag-47019/).

Icons in `./icons` are licensed under a
Creative Commons Attribution 3.0 Unported License.

You should have received a copy of the license along with this work.
If not, see <http://creativecommons.org/licenses/by/3.0/>.
